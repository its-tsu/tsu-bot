import React, {Component} from 'react';
import './style.scss';
import ThemeContext from "../../services/context"
import Bot from "../pages/Bot";

class App extends Component{

    constructor(props){
        super(props);
        this.state = {
          lang: 'ru'
        };
    }

    changeLang = () =>{
        this.setState((state)=>({
            lang: state.lang==='ru' ? 'en' : 'ru'
        }))
    };

    render() {
        const {lang} = this.state;
        return (
            <ThemeContext.Provider value={lang}>
                <Bot changeLang={this.changeLang}/>
            </ThemeContext.Provider>
        )
    }
}

export default App;
