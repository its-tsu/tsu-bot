import React, {Component} from 'react';
import Message from "../../../../components/pages/Bot/Chat/Message";
import Button from "../../../../components/pages/Bot/Chat/Button";
import socketIOClient from "socket.io-client";
import styled from "styled-components";
import ThemeContext from "../../../../services/context";
import {bot as texts} from '../../../../translates/index'

const socket = socketIOClient('https://tsu-tlt-bot.herokuapp.com/');

const lang = {
    'ru': 'ru-RU',
    'en': 'en-US'
};

class Chat extends Component {

    static contextType = ThemeContext;

    constructor(props) {
        super(props);

        this.state = {
            messages: [],
            you: '',
            bot: '',
            error: '',
            isListening: false,
            inputValue: '',
        };

        this.chatContainer = React.createRef();


        try {
            this.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
            this.recognition = new this.SpeechRecognition();
            this.recognition.interimResults = false;
            this.recognition.maxAlternatives = 1;
        } catch (e) {

            this.state.messages = [...this.state.messages, {
                who: 'error',
                text: 'Voice input is not supported!',
                key: this.state.messages.length
            }]

        }

    }

    componentDidMount() {
        try {
            this.recognition.lang = lang[this.context];
            this.recognition.addEventListener('speechstart', () => {
                console.log('Speech has been detected.');
            });

            this.recognition.addEventListener('result', (e) => {
                console.log('Result has been detected.');

                let last = e.results.length - 1;
                let text = e.results[last][0].transcript;

                this.setState({
                    messages: [...this.state.messages, {
                        who: 'you',
                        text: text,
                        key: this.state.messages.length
                    }]
                });
                console.log('Confidence: ' + e.results[0][0].confidence);

                socket.emit('chat message', text);
            });

            this.recognition.addEventListener('error', (e) => {
                this.setState({error: `Error: ${e.error}`});
            });

            this.recognition.addEventListener('end', () => {
                this.setState({isListening: false})
            });
        } catch (e) {
            this.setState({
                messages: [...this.state.messages, {
                    who: 'error',
                    text: texts[this.context].chat.noSupportMicro,
                    key: this.state.messages.length
                }]
            });
        }


        socket.on('bot reply', (replyText) => {
            this.synthVoice(replyText);

            if (replyText === '') replyText = '(No answer...)';
            this.setState({
                messages: [...this.state.messages, {
                    who: 'bot',
                    text: replyText,
                    key: this.state.messages.length
                }]
            });
        });

        socket.on('disconnect', () => {
            this.setState({
                messages: [...this.state.messages, {
                    who: 'error',
                    text: 'error',
                    key: this.state.messages.length
                }]
            });
        });

        this.setState({
            messages: [...this.state.messages, {
                who: 'bot',
                text: texts[this.context].chat.helloMessage,
                key: this.state.messages.length
            }]
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.messages !== this.state.messages) {
            try {
                this.chatContainer.current.scrollTo({top: this.chatContainer.current.scrollHeight, behavior: "smooth"});
            } catch (e) {

            }
        }
    }

    synthVoice = (text) => {
        const synth = window.speechSynthesis;
        const utterance = new SpeechSynthesisUtterance();
        utterance.text = text;
        synth.speak(utterance);
    };

    send = (e) => {
        const {inputValue} = this.state;
        e.preventDefault();
        if (inputValue) {
            this.setState(state => ({
                messages: [...state.messages, {
                    who: 'you',
                    text: inputValue,
                    key: state.messages.length
                }], inputValue: ''
            }));
            socket.emit('chat message', inputValue);
        }
    };

    onMicrophone = () => {
        try {
            this.recognition.start();
            this.setState({isListening: true})
        } catch (e) {
            this.setState(state => ({
                messages: [...state.messages, {
                    who: 'error',
                    text: texts[this.context].chat.onOffMicroError,
                    key: state.messages.length
                }]
            }));
        }
    };

    offMicrophone = () => {
        try {
            this.recognition.stop();
            this.setState({isListening: false})
        } catch (e) {
            this.setState(state => ({
                messages: [...state.messages, {
                    who: 'error',
                    text: texts[this.context].chat.onOffMicroError,
                    key: state.messages.length
                }]
            }));
        }

    };


    render() {
        const {error, messages, isListening, inputValue} = this.state;
        const {changeLang} = this.props;
        const chatTexts = texts[this.context].chat;
        return (
            <Container>
                <Error>{error}</Error>
                <List ref={this.chatContainer} className='chat'>
                    {
                        messages.map(message => {
                            return <Message
                                message={message}
                                key={message.key}/>
                        })
                    }
                </List>
                <div>
                    <Form onSubmit={(e) => this.send(e)}>
                        <Input
                            placeholder={chatTexts.placeholder}
                            value={inputValue}
                            onChange={(e) => {
                                this.setState({inputValue: e.target.value})
                            }}/>
                        <Button
                            type='submit'
                            typeBtn='send'>
                        </Button>
                    </Form>
                    <Flex>
                        {/*<Button typeBtn='toggle-lang' type='button' active={this.context==='ru'} onClick={changeLang}>ru</Button>*/}
                        {/*<Button typeBtn='toggle-lang' type='button' active={this.context==='en'} onClick={changeLang}>en</Button>*/}
                        <Button
                            typeBtn='toggle-micro'
                            onClick={isListening ? this.offMicrophone : this.onMicrophone}
                            type='button'>
                            {isListening ? chatTexts.offMicro : chatTexts.onMicro}
                        </Button>
                    </Flex>

                </div>
            </Container>
        );
    }
}

export default Chat;


const Container = styled.div`
  max-width: 400px;
  width: 100%;
  //max-height: 500px;
  min-height: 200px;
  flex-grow: 1;
  box-shadow: 0 0 10px rgba(0, 0, 0, .3);
  border-radius: 10px;
  position: relative;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  background: white;
`;

const Flex = styled.div`
  display: flex;
`;


const Error = styled.div`
   
`;

const List = styled.div`
   display: flex;
  flex-direction: column;
  padding: 20px;
  box-sizing: border-box;
  flex-grow: 1;
  overflow: auto;
`;

const Input = styled.input`
    display: flex;
  flex-grow: 1;
  padding: 5px;
  box-sizing: border-box;
  border: 1px solid #4285F4;
`;

const Form = styled.form`
    display: flex;
  height: 30px;
`;
