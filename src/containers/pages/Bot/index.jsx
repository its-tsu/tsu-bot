import React, {Component} from 'react';
import ThemeContext from "../../../services/context"
import Chat from "./Chat";
import styled from "styled-components";
import logo from '../../../images/logo.png';


class Bot extends Component {


    static contextType = ThemeContext;

    render() {
        const {changeLang} = this.props;
        return <Container>
            {/*<Title >*/}
            {/*    Hello! I'm the same bot that I was before, but now I'm written in <ReactWord>React JS</ReactWord>*/}
            {/*    .*/}
            {/*</Title>*/}
            <Logo src={logo}/>
            <Chat changeLang={changeLang}/>
        </Container>
    }
}

export default Bot;

const Container = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 10px;
  box-sizing: border-box;
`;

const Logo = styled.img`
  max-width: 400px;
  width: 100%;
  margin-bottom: 20px;
`;
