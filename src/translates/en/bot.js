export default  {
    chat: {
        onMicro: 'Turn on the microphone',
        offMicro: 'Turn off the microphone',
        helloMessage: 'Привет! Я Шурик - бот для помощи в поиске информации о ТГУ.',
        noSupportMicro: 'Ваш браузер не поддерживает голосовой ввод',
    }
}
