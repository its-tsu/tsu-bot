import botEN from './en/bot'
import botRU from './ru/bot'

export const bot = {
    en:botEN,
    ru:botRU
};
