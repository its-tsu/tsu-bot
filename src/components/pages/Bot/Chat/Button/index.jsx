import React, {Component} from 'react';
import styled from 'styled-components';

class Button extends Component {
    render() {
        const {typeBtn, type, onClick, children, active} = this.props;
        switch (typeBtn) {
            case 'toggle-micro':
                return <ToggleMicro
                    className='toggle'
                    onClick={onClick}
                    type={type}>
                    {children}
                </ToggleMicro>;

            case 'send':
                return  <Send type={type}> > </Send>;

            case 'toggle-lang':
                return <Lang className={active && 'active'}  onClick={!active ? onClick : undefined}>{children}</Lang>;

            default:
                return <button>{children}</button>
        }
    }
}

export default Button;

const ToggleMicro = styled.button`
  height: 50px;
  width: 100%;
  border: none;
  border-top: 1px solid white;
  background: #4285F4;
  color: white;
  font-size: 20px;
`;

const Send = styled.button`
  border: none;
  font-size: 25px;
  background: #4285F4;
  color: white;
`;

const Lang = styled.button`
  border: none;
  &.active{
     
  }
`;
