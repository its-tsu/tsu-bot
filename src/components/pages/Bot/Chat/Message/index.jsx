import React, {Component} from 'react';
import styled from "styled-components";

class Message extends Component {
    render() {
        const {message} = this.props;
        return <MessageContainer className={message.who}>
            <Text className={message.who} dangerouslySetInnerHTML={{__html:message.text}}/>
        </MessageContainer>

    }
}

export default Message;

const MessageContainer = styled.div`
   display: flex;
   margin-bottom: 10px;
  
   &.you{
       justify-content: flex-end;
   }
   
   &.bot, &.error{
        justify-content: flex-start;
   }
`;

const Text = styled.p`
    max-width: 90%;
  padding: 5px;
  box-sizing: border-box;
  border-radius: 10px;
  margin: 0;
  word-break: break-word;
  
   &.you{
        background: rgba(63,114,155,0.4);
   }
   
   &.bot{
         background: rgba(66,133,244,0.4);
   }
   
    &.error{
         background: rgba(250,43,90,0.4);
    }
   
    a {
      text-decoration: none;
      color: #4285F4;
    }

    img {
      max-width: 200px;
      margin-top: 10px;
    }
  
`;
